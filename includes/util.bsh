/*
	Taken from Laza - Thanks!
	Base skin in /includes/util.bsh
*/


// Retrieves an array of photo data fields on the following format:
// "Key1|Key1alias1|Key1alias2, Key2, Key3"
// Retruns the HTML formatted collection of fields found

String getPhotoData() {
	return (isEmpty(photoDataTemplate))? "" : getPhotoData(photoDataTemplate);
}

String getPhotoData(String templ, AlbumObject ao) {
	StringBuilder sb = new StringBuilder(1024);
	String key, val;
	for (String key : templ.split("[,;]")) {
		if ((val = getKey(key, ao)) != null) {
			if (sb.length() > 0) sb.append(sep);
			sb.append(val);
		}
	}
	return sb.toString();
}

String getKey(String templ, AlbumObject ao) {									// Returns the first found in a set of keys, separated by '|'
	String[] keys = templ.split("\\|");
	String label;
	Object val;
	for(String key : keys) {
		key = key.trim();
		if (key.charAt(0) == '#') continue;						// commented out key
		if ((val = getVar(key, ao)) != null && val.length() > 0) {	// getting the variable
			if (showMetaLabel) {								// Show with label
				label = getText(remapKey(key));					// Translated label
				escapedVal = val.replaceAll("\\\"","&quot;");
				return (isEmpty(label)? key : label) + ":<b> " + escapedVal+"</b><br>";
			}
			else												// Only the value
				return val;
		}
	}
	return null;
}

String getVar(String name, AlbumObject ao) {
	Object o;
	String s;
	String fileTitle = ao.getVars().get("filteTitle");
	String label = ao.getVars().get("label");
	Long fileSize = ao.getVars().get("fileSize");
	String pageType = ao.getVars().get("pageType");
	Integer imageNum = ao.getVars().get("imageNum");
	Integer _imageNum = ao.getVars().get("_imageNum");
	
	if (isEmpty(name)) {
		return null;
	}
	if (name.equals("fileLabel")) {
		return (fileTitle != void && !isEmpty(fileTitle))? fileTitle : label.replaceAll("_"," ");
	} 
	else if (name.equals("fileSize")) {
		return getSizeAsString(fileSize);
	}
	else if (name.equals("imageNum")) {
		return ((pageType.equals("slide") || _imageNum == void || _imageNum == null)? imageNum : _imageNum).toString();
	}
	else if ((o = getObject(name, ao)) != null) {
		return filterJunk(o.toString());
	} else if ((s = getMeta(name, ao)) != null) {
		return filterJunk(s);
	}
	return null;
}

String getSizeAsString(long s) {
	NumberFormat df = new DecimalFormat("0.00");
	Float n;
	String p;
	if (s >= 1073741824L) { n = s / 1073741824L; p = "GB"; }
	else if (s >= 1048576L) { n = s / 1048576L; p = "MB"; }
	else if (s >= 1024) { n = s / 1024L; p = "kB"; }
	else { n = s; p = "B"; }
	return df.format(n) + " " + p;
}

String filterJunk(int s) {
	return s.toString();
}

String filterJunk(String s) {
	if ( s == null ) {
		return s;
	}
	s = s.trim();
	if ( s.length() == 0 || s.equals("x") ||
		s.startsWith("ACD Systems Digital Imaging") ||
		s.startsWith("LEAD Technologies") ||
		s.startsWith("AppleMark") ||
		s.startsWith("Intel(R) JPEG Library") ||
		s.startsWith("Created with The GIMP") ||
		s.startsWith("ASCII") ||
		s.startsWith("OLYMPUS DIGITAL CAMERA") ||
		s.startsWith("File written by Adobe Photoshop")) {
		return "";
	}
	else if (s.startsWith("Flash did not fire")) {
		return noFlash;
	}
	return s;
}

Object getObject(String name, AlbumObject ao) {
	Object o;
	if ((ao != void && ao != null && (o = ao.getVars().get(name)) != null) ||
		(o = engine.getSkinVariables().get(name)) != null)
		return o;
	return null;
}

String getMeta(String key, AlbumObject ao) {

	Map meta = ao.getVars().get("meta");
	Object o;
	String v;
	if (key != null && meta != void && meta != null && 
		((o = meta.get(key)) != null ||
		(o = meta.get("Xmp." + key)) != null ||
		(o = meta.get("GPS." + key)) != null ||
		(o = meta.get("Iptc." + key)) != null ||
		(o = meta.get("Windows XP " + key)) != null)) {
		v = o.toString();
		// F-number correction ( Philippe Couton )
		if ("F-Number".equals(key) || "FNumber".equals(key) || 
			key.toLowerCase().startsWith("aperture")) {
			if (v.charAt(0) == 'F') {
				v = v.substring(1);
			}
			return "f/" + v;
		}
		return v;
	}
	return null;
}	

boolean isEmpty(Object o) {
	return o == void || o == null || o.toString().trim().equals("");
}

String remapKey(String s) {
	return s.replaceAll("Canon Makernote.Unknown tag (0x0095)", "lens");
}

String getText(String name) {
	String s = "";
	try { 
		s = texts.getString(name);
	} catch (MissingResourceException e) {
		return name;
	}
	return s;
}

String noFlash = getText("noFlash");
if ( isEmpty(noFlash) ) {
	noFlash = "No";
}
