		<%-- Custom Page --%> 
		<ja:if test="${pageType}" value="page"></article></ja:if>
		
		<div id="sidebar" class="left-top">
			<%-- Logo --%>
			<ja:if exists="logoLinkUrl">
				<a href="<%= ((!logoLinkUrl.toLowerCase().matches("\\p{L}*://.*"))? "http://" : "") %>${logoLinkUrl}">
			</ja:if>
			<ja:if exists="usedLogo">
				<img id="logo" src="${usedLogo}">
			</ja:if>
			<ja:if exists="logoLinkUrl">
				</a>
			</ja:if>
			<%-- Folders --%>
			<ja:if not test="${hideFoldersCompletely}">
			<div id="folders">
				<h2>
					<ja:if test=<%= level > 0 || fileCategory == Category.webPage %>> 
						<a href="${rootPath}/${indexName}">${homeText}</a>
					</ja:if><ja:else>
						<a href="${rootPath}/${indexName}" class="folderTitle">${homeText}</a>
					</ja:else>
				</h2>
				<%= getFoldersAndWebPages(currentFolder) %>	
				<ja:if test="${externalLink}">
				<h2>
					<a href="<%= ((!linkURL.toLowerCase().matches("\\p{L}*://.*"))? "http://" : "") %>${linkURL}">${linkTitle}</a>
				</h2>
				</ja:if>
			</div>
			</ja:if>	
		</div>
		
		<%-- Sharing, Credits, links --%>
		<div id="links" class="left-bottom">		
			<div class="sharing">
			</div>
			<ja:if exists="homepageAddress">
				<a id="homepageLink" href="${homepageAddress}"><%= homepageLinkText.equals("") ? homepageAddress : homepageLinkText %></a>
			</ja:if>
			<ja:if not test="${excludeBacklinks}">
			<div id="jalbum">
				<a href="${generatorUrl}" title="${generator}"><ja:if exists="creditText">${creditText}</ja:if><ja:else>jAlbum</ja:else></a> &amp; 
				<a href="http://jalbum.net/skins/skin/${skin}" title="${skin} ${skinVersion} :: ${styleName}">${skin} skin</a>
			</div>
			</ja:if>
		</div>
		
		<%-- Index Page: scripts included only on index pages --%> 
		<ja:if test="${pageType}" value="index">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
		<script>window.jQuery || document.write('<script src="${resPath}/jquery-1.10.2.min.js"><\/script>');</script>
		<script src="${resPath}/jquery-migrate-1.2.1.min.js"></script>
		
		<script src="${resPath}/jquery.touchSwipe.min.js"></script>		
		<script src="${resPath}/main.js"></script>
		<script>
		
			// Fully version: ${skinVersion}

			if( $("html").hasClass("ie8") || $("html").hasClass("ie7") ) {

			 	document.open();

			 	document.body.innerHTML = '';

				var html = '<ht'+'ml><he'+'ad><sty'+'le type="text/css">body { padding:3em 2em; } body,a { text-align:center; color: white; background-color: black; } </st'+'yle></he'+'ad>'; 

				html += '<bo'+'dy><h1>Sorry!</h1>'; 
				
				html += '<p>This fancy album needs modern features in your web browser - it will not work with this one.</p>'; 
				
				html += '<p>Just use anything newer than Internet Explorer 8, like '; 
				
				html += '<a href="http://www.mozilla.org/firefox" target="_blank">Firefox</a>, '; 
				
				html += '<a href="http://www.google.com/chrome" target="_blank">Chrome</a> or '; 
				
				html += '<a href="http://www.opera.com" target="_blank">Opera</a>.'; 
				
				html += '<p>Thanks!</p>' 
				
				html += '</b'+'ody></h'+'tml>'; 
				
				document.write(html); 

			 	document.close();
			 	
 			}

 			else {
				
				$(window).load(function(){
					initSkin({
						animationInterval: ${animationInterval},
						amountImages: ${totalImages},
						wrapAround: ${wrapAround},
						fontSize: ${fontSize},
						showPhotoDataFromStart: ${showPhotoDataFromStart},
						slideshowInterval: ${slideshowInterval},
						opacity: ${opacity},
						facebook: ${facebook},
						twitter: ${twitter},
						google: ${google},
						tumblr: ${tumblr},
						reddit: ${reddit},
						pinterest: ${pinterest},
						email: ${email},
						allowDownload: ${allowDownload}
					});
				});
			}

		</script>
		</ja:if>
	</body>
</html>
