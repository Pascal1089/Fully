<!DOCTYPE html>
<%
	// Initializing variables for the current album page
	initPage();
%>
<!--[if lt IE 8]><html ${htmlAttr} class="no-js ie7 oldie"><![endif]-->
<!--[if IE 8]><html ${htmlAttr} class="no-js ie8 oldie"><![endif]-->
<!--[if IE 9]><html ${htmlAttr} class="no-js ie9"><![endif]-->
<!--[if gt IE 9]><!--> <html ${htmlAttr} class="no-js"> <!--<![endif]-->
	<head>
		<meta name="viewport" content="initial-scale=1.0">
		<meta property="og:title" content="${title}"/>
		<meta charset="${textEncoding}">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title>${title}</title>
		<ja:if exists="description"><meta name="description" content="${description}"></ja:if>
		<ja:if exists="writer"><meta name="author" content="${writer}"></ja:if>
		<meta name="keywords" content="<ja:if test="${pageType}" value="slide">${keywords}</ja:if><ja:else>${tags}</ja:else>"> 
		<title>${title}</title>
		<link rel="stylesheet" href="${resPath}/common.css?v=${skinVersion}">
		<script src="${resPath}/modernizr-2.6.2.min.js"></script>
		<script src="${resPath}/ios-orientationchange-fix.js"></script>
		
		<ja:if exists="googleSiteID"><script> <%-- Google Analytics code --%>
			var _gaq=_gaq||[];_gaq.push(['_setAccount','${googleSiteID}']);_gaq.push(['_trackPageview']);
			(function(d){var ga=d.createElement('script');ga.async=true;ga.src=('https:'==d.location.protocol?'https://ssl':'http://www')+'.google-analytics.com/ga.js';
				var s=d.getElementsByTagName('script')[0];s.parentNode.insertBefore(ga,s);
			})(document);
		</script></ja:if>
		<!--[if lt IE 9]>
   
		   <style type="text/css">
		
		   #exifInfo, #exifCollapse { 
		       background:transparent;
		       filter:progid:DXImageTransform.Microsoft.gradient(startColorstr=#BD000000,endColorstr=#BD000000); 
		       zoom: 1;
		    } 
		
		    </style>
		
		<![endif]-->
	</head>
	<body>
		<ja:if test="${pageType}" value="page"><article></ja:if>
